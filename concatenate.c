#include <stdio.h> // standard input output library
#include <string.h>

int main() {
  char name[] = "Santi";
  char lastName[] = "Sanchez";
  char fullName[100];
  // strcat(name, lastName);
  // printf("%s\n", name);

  int i = 0;
  while (i < strlen(name)) {
    fullName[i] = name[i];
    i++;
  }

  fullName[i] = ' ';
  i++;

  int j = 0;
  while (j < strlen(lastName)) {
    fullName[i] = lastName[j];
    i++;
    j++;
  }
  fullName[i] = '\0';

  printf("%s\n", fullName);
  return 0;
}
