#include <stdio.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

int main() {
  int fileDescriptor[2];
  int status;
  char buffer[16];

  pid_t i = fork();

  // if (i == 0) {
  //   read(fileDescriptor[0], buffer, 7);
  //   printf("%s\n", buffer);
  // } else {
  //   write(fileDescriptor[1], "hello ", 6);
  //   write(fileDescriptor[1], "hola\n", 5);
  //   write(fileDescriptor[1], "world", 5);
  //   // waitpid(i, &status, 0);
  //   wait(&status);
  // }

  if (i == 0) {
    write(fileDescriptor[1], "hello ", 6);
    write(fileDescriptor[1], "hola\n", 5);
    write(fileDescriptor[1], "world", 5);
  } else {
    waitpid(i, &status, 0);
    // wait(&status);
    read(fileDescriptor[0], buffer, 16);
    printf("%s\n", buffer);
  }

  return 0;
}
