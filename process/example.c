#include <stdio.h>
#include <sys/wait.h>
#include <unistd.h>

int main() {
  pid_t pid;
  int status;

  pid = fork();

  if (pid == -1) {
    printf("Error: Unable to create child process\n");
  } else if (pid == 0) {
    // Child process
    printf("Child process with pid %d\n", getpid());
    printf("Child process exiting\n");
  } else {
    // Parent process
    printf("Parent process with pid %d\n", getpid());
    printf("Waiting for child process to complete...\n");
    wait(&status); // wait any child ends to continue
    printf("Child process exited with status %d\n", WEXITSTATUS(status));
  }

  return 0;
}
