#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>

int main() {
  int fileDescriptor[2];
  char buffer[16];

  if (pipe(fileDescriptor) == -1) {
    printf("Error\n");
    return 2;
  }

  write(fileDescriptor[1], "hello ", 6);
  write(fileDescriptor[1], "hola\n", 5);
  write(fileDescriptor[1], "world", 5);

  read(fileDescriptor[0], buffer, 16);
  printf("%s\n", buffer);
  return 0;
}
