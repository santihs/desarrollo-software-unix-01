#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>

int main() {
  for (size_t i = 0; i < 2; i++) {
    pid_t id = fork();
    if (id == 0) {
      break;
    }
    printf("PID: %d, PPID: %d, ID: %d\n", getpid(), getppid(), id);
  }

  return 0;
}
