#include <stdio.h>
#include <sys/wait.h>
#include <unistd.h>

int main(int argc, char *argv[]) {
  int pid = fork();

  if (pid == -1) {
    return 1;
  }
  if (pid == 0) {
    while (1) {
      printf("Child process is working...\n");
      usleep(60000);
    }
  } else {
    sleep(5);
    kill(pid, SIGKILL);
    wait(NULL);
  }
  return 0;
}
