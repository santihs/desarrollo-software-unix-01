#include <stdio.h>
#include <sys/wait.h>
#include <unistd.h>

int main(int argc, char *argv[]) {
  int pid = fork();
  int counter = 0;

  if (pid == -1) {
    return 1;
  }
  if (pid == 0) {
    while (1) {
      printf("Child process is working %d...\n", counter);
      usleep(60000);
      counter++;
    }
  } else {
    kill(pid, SIGSTOP);
    int secs;
    do {
      printf("enter child process execution time (seconds): \n");
      scanf("%d", &secs);
      if (secs > 0) {
        kill(pid, SIGCONT);
        sleep(secs);
        kill(pid, SIGSTOP);
      }
    } while (secs > 0);

    kill(pid, SIGKILL);
    wait(NULL);
  }
  return 0;
}
