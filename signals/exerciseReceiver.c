#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <sys/shm.h>
#include <sys/types.h>
#include <unistd.h>

#define TEXT_LIMIT 50

struct msg_struct {
  long int msg_type;
  char msg_text[TEXT_LIMIT];
};

int main() {

  int messaging = 1;
  int msg_id;
  char my_buffer[20];
  long int msg_receiver = 0;
  struct msg_struct my_msg;

  msg_id = msgget((key_t)123123, 0666 | IPC_CREAT);

  if (msg_id == -1) {
    printf("Error\n");
    return 1;
  }

  while (messaging) {

    msgrcv(msg_id, (void *)&my_msg, TEXT_LIMIT, msg_receiver, 0);

    printf("Message received: %s\n", my_msg.msg_text);

    printf("Message: \n");
    fgets(my_buffer, 20, stdin);

    my_msg.msg_type = 1;
    strcpy(my_msg.msg_text, my_buffer);

    if (msgsnd(msg_id, (void *)&my_msg, TEXT_LIMIT, 0) == -1) {
      printf("Msg Error\n");
      return 2;
    }

    if (strncmp(my_msg.msg_text, "bye", 3) == 0) {
      messaging = 0;
    }
  }
  msgctl(msg_id, IPC_RMID, 0);
  return 0;
}
