#include <signal.h>
#include <stdio.h>
#include <sys/wait.h>
#include <unistd.h>

void handle_sigtstp() { printf("You can't stop the process\n"); }
void handle_sigstp() { printf("Again, hello world\n"); }

int main() {
  struct sigaction sa;

  signal(SIGTSTP, &handle_sigtstp);
  // signal(SIGSTOP, &handle_sigtstp);
  // signal(SIGCONT, &handle_sigstp);

  int age;
  printf("What is your age?: \n");
  scanf("%d", &age);

  printf("Hwllo world\n");

  return 0;
}
