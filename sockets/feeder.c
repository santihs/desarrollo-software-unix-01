#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>

#include <sys/socket.h>
#include <sys/types.h>

#include <netinet/in.h>
#include <string.h>
#include <unistd.h>

int main() {
  int feeder_fd;

  // Create socket
  feeder_fd = socket(AF_INET, SOCK_STREAM, 0);

  if (feeder_fd == -1) {
    printf("CLIENT: socket error\n");
    return 1;
  } else {
    printf("CLIENT: socket created successfully.\n");
  }

  // Define server address
  struct sockaddr_in accumulator_address;

  memset(&accumulator_address, 0, sizeof(accumulator_address));
  accumulator_address.sin_family = AF_INET;
  accumulator_address.sin_port = htons(9000);
  // accumulator_address.sin_addr.s_addr = INADDR_ANY;
  accumulator_address.sin_addr.s_addr = inet_addr("172.20.177.10");

  // Connect to IP and port
  if (connect(feeder_fd, (struct sockaddr *)&accumulator_address,
              sizeof(accumulator_address)) == -1) {
    printf("CLIENT: connect error\n");
    return 2;
  } else {
    printf("CLIENT: connection stablished with accumulator\n");
  }

  int number = 10;
  int total;
  char c = 'x';

  // Send message
  if (send(feeder_fd, &c, sizeof(number), 0) == -1) {
    printf("CLIENT: send error\n");
    return 5;
  }

  // if (recv(feeder_fd, &total, sizeof(total), 0) == -1) {
  //   printf("CLIENT: recv error\n");
  //   return 6;
  // } else {
  //   printf("CLIENT: received total from accumulator (total = %d)\n", total);
  // }

  // Close client socket
  close(feeder_fd);

  return 0;
}
