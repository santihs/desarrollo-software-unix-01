#include <stdio.h>
#include <stdlib.h>

#include <sys/socket.h>
#include <sys/types.h>

#include <arpa/inet.h>
#include <errno.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>

int main() {

  int accumulator_fd;

  accumulator_fd = socket(AF_INET, SOCK_STREAM, 0);

  if (accumulator_fd == -1) {
    printf("SERVER: socket error\n");
    return 1;
  } else {
    printf("SERVER: socket created successfully.\n");
  }

  // Define server address
  struct sockaddr_in accumulator_address;

  memset(&accumulator_address, 0, sizeof(accumulator_address));
  accumulator_address.sin_family = AF_INET;
  accumulator_address.sin_port = htons(9000);
  accumulator_address.sin_addr.s_addr = inet_addr("172.20.177.10");

  // bidn the socket to the IP and PORT
  if (bind(accumulator_fd, (struct sockaddr *)&accumulator_address,
           sizeof(accumulator_address)) == -1) {
    printf("SERVER: bind error\n");
    return 2;
  } else {
    printf("SERVER: bound to IP and PORT 9000.\n");
  }

  // Listen - make socket to be a passive one
  if (listen(accumulator_fd, 5) == -1) {
    printf("SERVER: listen error\n");
    return 3;
  } else {
    printf("SERVER: socket is passive\n");
  }

  // Wait for client
  int client_socket_fd;
  int len = sizeof(accumulator_address);
  int number;
  int total;
  char c;

  do {
    client_socket_fd =
        accept(accumulator_fd, (struct sockaddr *)&accumulator_address,
               (socklen_t *)&len);

    if (client_socket_fd == -1) {
      printf("SERVER: accept error\n");
      return 4;
    } else {
      printf("SERVER: waiting for client connections\n");
    }

    if (recv(client_socket_fd, &c, sizeof(number), 0) == -1) {
      printf("SERVER: recv error\n");
      return 5;
    } else {
      printf("SERVER: received number from client (number = %c)\n", c);
    }

    total += number;

    c -= 1;
    // Send message
    if (send(client_socket_fd, &c, sizeof(total), 0) ==
        -1) { // Warning according to Nicolas
      printf("SERVER: send error\n");
      return 6;
    } else {
      printf("SERVER: sends total to client (total = %c)\n", c);
    }

    // Close server socket
    close(client_socket_fd);

  } while (c != 'a');

  // Close server socket
  close(accumulator_fd);

  return 0;
}
