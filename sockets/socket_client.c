// ipc_socket_Client
#include <arpa/inet.h>
#include <errno.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>

int main(int argc, char const *argv[]) {
  int client_socket_fd;
  // Create socket

  client_socket_fd = socket(AF_INET, SOCK_STREAM, 0);

  if (client_socket_fd == -1) {
    printf("CLIENT: socket error\n");
    return 1;
  }
  // Define server address
  struct sockaddr_in server_address;
  server_address.sin_family = AF_INET;
  server_address.sin_port = htons(9000);
  // server_address.sin_addr.s_addr = INADDR_ANY;
  server_address.sin_addr.s_addr = inet_addr("172.20.177.233");
  // Connect to IP and port
  if (connect(client_socket_fd, (struct sockaddr *)&server_address,
              sizeof(server_address)) == -1) {
    printf("CLIENT: connect error\n");
    return 2;
  }
  // Received message
  char server_response[256];
  recv(client_socket_fd, &server_response, sizeof(server_response), 0);
  printf("RESPONSE: %s\n", server_response);
  // close client socket

  close(client_socket_fd);

  return 0;
}
