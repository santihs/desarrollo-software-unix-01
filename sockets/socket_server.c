#include <arpa/inet.h>
#include <errno.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>

int main() {
  char server_response[256] = "hello, I'm the server ...";
  int server_socker_fd;

  server_socker_fd = socket(AF_INET, SOCK_STREAM, 0);

  if (server_socker_fd == -1) {
    printf("SERVER: socket error\n");
    return 1;
  } else {
    printf("SERVER: socker created\n");
  }

  struct sockaddr_in server_address;

  memset(&server_address, 0, sizeof(server_address));
  server_address.sin_family = AF_INET;
  server_address.sin_port = htons(9000);
  server_address.sin_addr.s_addr = inet_addr("172.20.177.10");

  if (bind(server_socker_fd, (struct sockaddr *)&server_address,
           sizeof(server_address)) == -1) {
    printf("Serveer: bind error\n");
    return 1;
  } else {
    printf("Server, bound to ip and port 9000\n");
  }

  if (listen(server_socker_fd, 5) == -1) {
    printf("SERVER: listen error\n");
    return 3;
  }

  int client_socket_fd;

  int len = sizeof(server_address);

  client_socket_fd = accept(
      server_socker_fd, (struct sockaddr *)&server_address, (socklen_t *)&len);

  if (client_socket_fd == -1) {
    printf("SERVER: accep error\n");
    printf("error %d\n", errno);
    return 4;
  }

  if (send(client_socket_fd, server_response, sizeof(server_response), 0) ==
      -1) {
    printf("SERVER: send error\n");
    return 5;
  }

  close(server_socker_fd);

  return 0;
}
