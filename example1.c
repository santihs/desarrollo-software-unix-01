#include <stdio.h> // standard input output library
#include <stdlib.h>

// NOTE:Directivas y variables
#define PI 3.141592

const int SIZE = 23;

int year = 2023;

int main(int argc, char *argv[]) {
  int age = 10;
  printf("Hello world\n");
  printf("%.2f\n", PI);

  // NOTE: Data input / output
  char c;
  // scanf("%c", &c);

  // printf("The character is %c\n", c);

  char name[20];
  char name2[100];

  // scanf("%s", name);

  // printf("The name is %s\n", name);

  fgets(name2, 100, stdin);

  printf("Name is: %s \n", name2);

  return 0;
}
