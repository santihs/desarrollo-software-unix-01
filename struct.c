#include <stdio.h> // standard input output library

struct position {
  int x;
  int y;
};

struct students {
  char name[100];
  char lastName[100];
  int age;
};

struct students fillStudent(struct students student) {
  printf("Introduce the name: \n");
  scanf("%s", student.name);
  printf("Introduce the lastname: \n");
  scanf("%s", student.lastName);
  printf("Introduce the age: \n");
  scanf("%d", &student.age);
  return student;
}

int main() {
  struct position vector = {2, 3};
  struct position vector2 = {3, 3};

  printf("Pos x: %d, Pos y: %d\n", vector.x, vector.y);
  printf("Pos x: %d, Pos y: %d\n", vector2.x, vector2.y);

  struct students first;
  struct students second;

  first = fillStudent(first);
  second = fillStudent(second);

  printf("Name: %s, lastName: %s, Age: %d\n", first.name, first.lastName,
         first.age);

  printf("Name: %s, lastName: %s, Age: %d\n", second.name, second.lastName,
         second.age);
  return 0;
}
