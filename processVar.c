
#include <stdio.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

int main() {
  int x = 4;

  if (fork() == 1) {
    x += 1;
    printf("pid: %d, x:%d\n", getpid(), x);
  }
  printf("pid: %d, x:%d\n", getpid(), x);

  return 0;
}
