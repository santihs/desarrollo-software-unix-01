#include <pthread.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

int x = 0;
char text[] = "eucalipto";
int counterA = 0;
int counterE = 0;
int counterI = 0;
int counterO = 0;
int counterU = 0;

void *runA() {
  for (size_t i = 0; i < strlen(text); i++) {
    char curr = text[i];
    if (curr == 'a') {
      counterA++;
    }
  }
  return NULL;
}

void *runE() {
  for (size_t i = 0; i < strlen(text); i++) {
    char curr = text[i];
    if (curr == 'e') {
      counterE++;
    }
  }
  return NULL;
}
void *runI() {
  for (size_t i = 0; i < strlen(text); i++) {
    char curr = text[i];
    if (curr == 'i') {
      counterI++;
    }
  }
  return NULL;
}
void *runO() {
  for (size_t i = 0; i < strlen(text); i++) {
    char curr = text[i];
    if (curr == 'o') {
      counterO++;
    }
  }
  return NULL;
}
void *runU() {
  for (size_t i = 0; i < strlen(text); i++) {
    char curr = text[i];
    if (curr == 'u') {
      counterU++;
    }
  }
  return NULL;
}

int main() {
  int i;
  pthread_t t1;
  pthread_t t2;
  pthread_t t3;
  pthread_t t4;
  pthread_t t5;

  pthread_create(&t1, NULL, runA, &i);
  pthread_create(&t2, NULL, runE, &i);
  pthread_create(&t3, NULL, runI, &i);
  pthread_create(&t4, NULL, runO, &i);
  pthread_create(&t5, NULL, runU, NULL);

  pthread_join(t1, NULL);
  pthread_join(t2, NULL);
  pthread_join(t3, NULL);
  pthread_join(t4, NULL);
  pthread_join(t5, NULL);

  printf("a:%d, e:%d, i:%d, o:%d, u:%d\n", counterA, counterE, counterI,
         counterO, counterU);

  return 0;
}
