#include <stdio.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

int main() {
  int x = 4;

  pid_t t = fork();
  if (t == 0) {
    x++;
    printf("id: %d, x:%d\n", getpid(), x);
  } else {
    wait(NULL);
    ++x;
    printf("id: %d, x:%d\n", getpid(), x);
  }

  return 0;
}
