#include <pthread.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

int x = 0;
int limit = 100000;
pthread_mutex_t mutex;

void *runtine() {
  for (int i = 0; i < limit; ++i) {
    /*if(flag ==1 ){
    sleep(1);
    }
    flag = 1;*/
    pthread_mutex_lock(&mutex);
    x++;
    pthread_mutex_unlock(&mutex);

    // flag = 0;
  }
  printf("x = %d,pid: %d \n", x, getpid());
  return NULL;
}

int main() {
  int i;
  pthread_t t1;
  pthread_t t2;

  pthread_mutex_init(&mutex, NULL);
  pthread_create(&t1, NULL, runtine, &i);
  pthread_create(&t2, NULL, runtine, &i);

  pthread_join(t1, NULL);
  pthread_join(t2, NULL);

  printf("id: %d, x:%d\n", getpid(), x);

  return 0;
}
