#include <pthread.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

int x = 0;

void *runtine() {
  x++;
  printf("id: %d, x:%d\n", getpid(), x);
}

int main() {
  int i;
  pthread_t t1;
  pthread_t t2;

  pthread_create(&t1, NULL, runtine, &i);
  pthread_create(&t2, NULL, runtine, &i);

  pthread_join(t1, NULL);
  pthread_join(t2, NULL);

  printf("id: %d, x:%d\n", getpid(), x);

  return 0;
}
