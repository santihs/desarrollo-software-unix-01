#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/types.h>
#include <unistd.h>

int main() {
  void *shared_memory;
  int sm_id;
  char myBuffer[100];

  sm_id = shmget((key_t)1234, 1024, 0666);

  if (sm_id < 0) {
    printf("Error\n");
  }

  shared_memory = shmat(sm_id, NULL, 0);

  printf("Hello %s\n", (char *)shared_memory);

  return 0;
}

